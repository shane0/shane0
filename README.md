# shane0

## Links

- [resume / cv](https://gitlab.com/shane0/shane0/-/blob/main/docs/shane-null-2022.pdf)
- [linkedin](https://www.linkedin.com/in/shanenull)
- [linktree](https://linktr.ee/shanenull)

## Screencast

Each year I upload a screencast of my personal workflow, 
[here is a link to the the video for 2022](https://youtu.be/nW9Okn6k6Qo).

---

Here's my mug.

![](https://gitlab.com/shane0/shane0/uploads/42f2fb4e8aa57ab0bd23c65659698b4f/c3d4c9ff317cf508ea79020e050a8b21.png)

